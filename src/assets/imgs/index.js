export { default as dblArrLeft } from './pagination/double-arr-left.svg';
export { default as dblArrRight } from './pagination/double-arr-right.svg';
export { default as arrLeft } from './pagination/arr-left.svg';
export { default as arrRight } from './pagination/arr-right.svg';