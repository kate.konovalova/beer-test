import styled, {createGlobalStyle} from 'styled-components';
import {NavLink} from "react-router-dom";

export const Global = createGlobalStyle`
  html {
    overflow-x: hidden;
  }
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap');
    font-family: 'Montserrat', sans-serif;
    font-weight: 400;
    font-size: 16px;
    color: #313441;
  }

  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
`

export const StyledHeader = styled.header`
  background-color: #DBEFF4;
  height: 80px;
  display: flex;
  align-items: center;
`

export const StyledLink = styled(NavLink)`
  font-size: 18px;
  font-weight: 600;
  text-decoration: none;
  margin-right: 30px;
  position: relative;
  &.active {
    opacity: 0.7;
  }
  &::before {
    position: absolute;
    bottom: -4px;
    content: '';
    height: 3px;
    width: 0;
    background-color: #313441;
    transition: all 0.3s;
  }
  &:hover {
    &::before {
      width: 100%;
    }
  }
`

export const Wrap = styled.div`
  max-width: 1180px;
  width: 100%;
  margin: 0 auto;
  padding: 0 20px;
`

export const StyledTable = styled.div`
  padding: 60px 0;
  margin-bottom: 50px;
  max-width: 1140px;
  width: 100%;
`

export const InnerWrap = styled.div`
  padding: 60px 0;
  margin-bottom: 50px;
  max-width: 1140px;
  width: 100%;
`

export const StyledPagination = styled.div`
  display: flex;
  list-style: none;
  margin: 30px -10px 50px;
  max-width: 1140px;
  width: 100%;
  li {
    height: 46px;
    width: 46px;
    margin: 0 10px;
    box-shadow: 4px 4px 15px rgba(0, 0, 0, 0.10);
    border-radius: 4px;
    background-color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    &:hover {
      background-color: #DBEFF4;
    }
    &.active {
      background-color: #5D46D9;
      color: white;
      font-weight: 600;
    }
  }
`

export const MainTitle = styled.div`
  margin-bottom: 30px;
  font-size: 24px;
  font-weight: 600;
`

export const StyledList = styled.div`
  margin-bottom: 30px;
  font-size: 24px;
  font-weight: 600;
  display: flex;
  flex-direction: column;
  justify-content: center;
  h4 {
    font-weight: 600;
    margin-bottom: 10px;
  }
  li {
    margin-left: 10px;
    margin-bottom: 5px;
    position: relative;
    &::before {
      position: absolute;
      content: '';
      background-color: #534CD8;
      height: 6px;
      width: 6px;
      border-radius: 3px;
      left: -10px;
      top: 7px;
      flex: none;
    }
  }
`

export const StyledTableRow = styled.li`
  max-width: 1140px;
  width: 100%;
  height: 230px;
  padding: 20px 30px;
  border-radius: 10px;
  margin-bottom: 10px;
  box-shadow: 4px 4px 15px rgba(0, 0, 0, 0.10);
  display: flex;
  align-items: center;
  justify-content: space-between;
  div {
    width: 10%;
    height: 100%;
    img {
      height: 100%;
      width: 100%;
      object-fit: contain;
    }
  }
  span {
    width: 15%;
  }
  div:last-child {
    width: 30%;
  }
`

export const StyledTableHead = styled.div`
  max-width: 1140px;
  width: 100%;
  height: 60px;
  padding: 0 30px;
  border-radius: 10px;
  background-color: #5D46D9;
  margin-bottom: 20px;
  ul {
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 100%;
  }

  li {
    display: flex;
    align-items: center;
    width: 15%;
    font-size: 18px;
    font-weight: 600;
    color: white;
    &:first-child {
      width: 10%;
    }
    &:last-child {
      width: 30%;
    }
  }

`