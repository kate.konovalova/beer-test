import React from 'react';
import PropTypes from 'prop-types';

//styles
import { StyledList } from './../../assets/styled-components';

const List = ({items, title}) => {
    return (
        <StyledList>
            {title && <h4>{title}</h4>}
            <ul>
                {items && items.map((item) => (
                    <li key={`${item}`}>{item}</li>
                ))}
            </ul>
        </StyledList>
    )
}

List.propTypes = {
    items: PropTypes.arrayOf(PropTypes.string),
    title: PropTypes.string
}

export default List;