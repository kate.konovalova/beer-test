import React from 'react';

//style
import { Wrap, StyledHeader, StyledLink } from '../../assets/styled-components';

const Header = () => {
    return (
        <StyledHeader>
            <Wrap>
                <StyledLink exact to="/" activeClassName="active">
                    Home
                </StyledLink>
                <StyledLink exact to="/table" activeClassName="active">
                    Table
                </StyledLink>
            </Wrap>
        </StyledHeader>
    );
}


export default Header;