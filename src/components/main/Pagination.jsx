import React from 'react';
import PropTypes from 'prop-types'

//imgs
import {dblArrLeft, dblArrRight, arrLeft, arrRight} from './../../assets/imgs'

//styles
import {StyledPagination} from './../../assets/styled-components';

const Pagination = ({pages, page, onClickPage}) => {
    const pagesNumbers = [];
    for (let i = 1; i <= pages; i++) {
        pagesNumbers.push(i)
    }
    const pagesList = pagesNumbers.map((item, i) => (
        <li key={`page_${i}`} className={page === i + 1 ? 'active' : ''} onClick={() => onClickPage(i + 1)}>{item}</li>
    ))
    return (
        <StyledPagination>
            {
                page > 1 && (
                    <>
                        <li onClick={() => onClickPage(1)}>
                            <img src={dblArrLeft} alt="dbl-arr-left"/>
                        </li>
                        <li onClick={() => onClickPage(--page)}>
                            <img src={arrLeft} alt="arr-left"/>
                        </li>
                    </>
                )
            }
            {
                pages && pagesList
            }
            {
                page !== pages && (
                    <>
                        <li onClick={() => onClickPage(++page)}>
                            <img src={arrRight} alt="arr-right"/>
                        </li>
                        <li onClick={() => onClickPage(pages)}>
                            <img src={dblArrRight} alt="dbl-arr-right"/>
                        </li>
                    </>
                )
            }
        </StyledPagination>
    )
}

Pagination.propTypes = {
    pages: PropTypes.number,
    page: PropTypes.number,
    onClickPage: PropTypes.func
};

export default Pagination;