import React, { useState, useEffect, useCallback} from 'react';
import { useDispatch, useSelector } from "react-redux";
import { getData } from '../redux/actions/table';

import TableHead from './table/TableHead';
import TableRow from './table/TableRow'
import { Pagination } from './../components';

//styles
import { StyledTable, MainTitle } from './../assets/styled-components';

const tableHeaderNames = ['Картинка', 'Название', 'Крепость', 'Блюда'];

const Table = () => {
    const dispatch = useDispatch();
    const {tableItems, perPage, pages, isLoading, isError} = useSelector(({table}) => {
        return {
            tableItems: table.items,
            perPage: table.perPage,
            pages: table.pages,
            isLoading: table.isLoading,
            isError: table.isError
        }
    });
    const [page, setPage] = useState(1);

    useEffect(() => {
        const url = `https://api.punkapi.com/v2/beers?page=${page}&per_page=${perPage}`
        dispatch(getData(url))
    }, [page, perPage, dispatch]);

    const onChangePage = useCallback((page) => {
        setPage(page);
    }, []);

    if (isError) {
        return <StyledTable>
            <MainTitle>Что-то пошло не так…</MainTitle>;
        </StyledTable>
    }

    if (isLoading) {
        return <StyledTable>
            <MainTitle>Загрузка…</MainTitle>;
        </StyledTable>
    }

    return (
        <StyledTable>
            <MainTitle>Ассортимент пива</MainTitle>
            <TableHead tableHeaderNames={tableHeaderNames}/>
            <div>
                <TableRow tableItems={tableItems}/>
            </div>
            <Pagination pages={pages} page={page} onClickPage={onChangePage}/>
        </StyledTable>
    );
}


export default Table;