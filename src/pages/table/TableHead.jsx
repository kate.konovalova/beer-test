import React from 'react';
import PropTypes from 'prop-types';

//styles
import { StyledTableHead } from './../../assets/styled-components'

const TableHead = ({ tableHeaderNames }) => {
    const content = tableHeaderNames.map((item, i) => (
        <li key={`${item}-${i}`}>
            {item}
        </li>
    ));

    return(
        <StyledTableHead>
            <ul>
                {tableHeaderNames && content}
            </ul>
        </StyledTableHead>
    )
}

TableHead.propTypes = {
    tableHeaderNames: PropTypes.arrayOf(PropTypes.string)
};

export default TableHead;