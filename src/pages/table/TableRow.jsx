import React from 'react';
import PropTypes from 'prop-types';

import { List } from '../../components';

//styles
import { StyledTableRow } from './../../assets/styled-components'


const TableRow = ({ tableItems }) => {
    const content = tableItems.map((item, i) => (
        <StyledTableRow key={`row__${item.id}`}>
            <div>
                <img src={item.image_url} alt={item.image_url}/>
            </div>
            <span>{item.name}</span>
            <span>{item.abv}</span>
            <List items={item.food_pairing} title={'Подходит для'}/>
        </StyledTableRow>
    ))

    return(
        <div className="table-row">
            <div>
                { tableItems && content }
            </div>
        </div>
    )
}

TableRow.propTypes = {
    tableItems: PropTypes.arrayOf(PropTypes.object)
}

export default TableRow;