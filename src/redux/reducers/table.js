import { SET_DATA, SET_LOADING, SET_ERROR } from './../actions/table';

const initialState = {
    pages: 10,
    perPage: 20,
    isLoading: false,
    isError: false,
    items: [],
}

const table = (state = initialState, action) => {
    switch (action.type) {
        case SET_DATA:
            return {
                ...state,
                items: action.payload
            }
        case SET_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case SET_ERROR:
            return {
                ...state,
                isError: action.payload
            }
        default:
            return state
    }
}

export default table;