export const SET_DATA = 'SET_DATA';
export const SET_LOADING = 'SET_LOADING';
export const SET_ERROR = 'SET_ERROR';

const getRequest = async(url, load, setSuccess, setError, dispatch) =>
{
    let items = [];
    dispatch(load(true));

    const response = await fetch(url);
    if (response.ok) {
        dispatch(load(false));
        items = await response.json();
        dispatch(setSuccess(items));

    } else {
        dispatch(load(false));
        dispatch(setError(true));
        throw Error(response.statusText);
    }
}

export const setData = (items) => {
    return {
        type: SET_DATA,
        payload: items
    }
}

export const setLoaded = (payload) => {
    return {
        type: SET_LOADING,
        payload
    }
}

export const setError = (payload) => {
    return {
        type: SET_ERROR,
        payload
    }
}

export const getData = (url) => {
    return (dispatch) => {
        getRequest(url, setLoaded, setData, setError, dispatch);
    }
}


