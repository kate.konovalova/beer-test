import { Route, Switch } from 'react-router-dom';


import { Header } from './components/index'
import { Home, Table } from './pages';

//styled-components
import { Wrap, Global } from './assets/styled-components'

function App() {
  return (
    <div>
      <Global/>
      <Header />
      <Wrap>
          <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/table" component={Table} />
          </Switch>
      </Wrap>
    </div>
  );
}

export default App;
